package parser.model.EZID;

public class OsobniJednani {

    private String zacatekDatum;
    private String zacatekCas;
    private String konecDatum;
    private String konecCas;
    private String adresaJednani;
    private String zpusobZaznamu;
    private String popisJine;

    public OsobniJednani() {
    }

    public String getZacatekDatum() {
        return zacatekDatum;
    }

    public void setZacatekDatum(String zacatekDatum) {
        this.zacatekDatum = zacatekDatum;
    }

    public String getZacatekCas() {
        return zacatekCas;
    }

    public void setZacatekCas(String zacatekCas) {
        this.zacatekCas = zacatekCas;
    }

    public String getKonecDatum() {
        return konecDatum;
    }

    public void setKonecDatum(String konecDatum) {
        this.konecDatum = konecDatum;
    }

    public String getKonecCas() {
        return konecCas;
    }

    public void setKonecCas(String konecCas) {
        this.konecCas = konecCas;
    }

    public String getAdresaJednani() {
        return adresaJednani;
    }

    public void setAdresaJednani(String adresaJednani) {
        this.adresaJednani = adresaJednani;
    }

    public String getZpusobZaznamu() {
        return zpusobZaznamu;
    }

    public void setZpusobZaznamu(String zpusobZaznamu) {
        this.zpusobZaznamu = zpusobZaznamu;
    }

    public String getPopisJine() {
        return popisJine;
    }

    public void setPopisJine(String popisJine) {
        this.popisJine = popisJine;
    }
}
