package parser.model.podpis;

public class Podpis {

    private PodpisHlavicka hlavicka;
    private PodpisOsoba isfo;
    private String dne;
    private String v;
    private PodpisOsoba dluznikFo;
    private PodpisOsoba dluznikManzele;

    public Podpis() {
        hlavicka = new PodpisHlavicka();
        isfo = new PodpisOsoba();
        dluznikFo = new PodpisOsoba();
        dluznikManzele = new PodpisOsoba();
    }

    public PodpisOsoba getIsfo() {
        return isfo;
    }

    public void setIsfo(PodpisOsoba isfo) {
        this.isfo = isfo;
    }

    public String getDne() {
        return dne;
    }

    public void setDne(String dne) {
        this.dne = dne;
    }

    public String getV() {
        return v;
    }

    public void setV(String v) {
        this.v = v;
    }

    public PodpisOsoba getDluznikFo() {
        return dluznikFo;
    }

    public void setDluznikFo(PodpisOsoba dluznikFo) {
        this.dluznikFo = dluznikFo;
    }

    public PodpisOsoba getDluznikManzele() {
        return dluznikManzele;
    }

    public void setDluznikManzele(PodpisOsoba dluznikManzele) {
        this.dluznikManzele = dluznikManzele;
    }

    public PodpisHlavicka getHlavicka() {
        return hlavicka;
    }

    public void setHlavicka(PodpisHlavicka hlavicka) {
        this.hlavicka = hlavicka;
    }
}
