package parser.model.podpis;

public class PodpisHlavicka {
    private String v;
    private String dne;

    public PodpisHlavicka() {
    }

    public String getV() {
        return v;
    }

    public void setV(String v) {
        this.v = v;
    }

    public String getDne() {
        return dne;
    }

    public void setDne(String dne) {
        this.dne = dne;
    }
}
