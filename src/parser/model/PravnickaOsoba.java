package parser.model;

public class PravnickaOsoba {

    private String zahranicni;
    private String nazev;
    private String ic;
    private String jineReg;
    private String pravniRad;
    private String sidlo;

    public PravnickaOsoba() {

    }

    public String getZahranicni() {
        return zahranicni;
    }

    public void setZahranicni(String zahranicni) {
        this.zahranicni = zahranicni;
    }

    public String getNazev() {
        return nazev;
    }

    public void setNazev(String nazev) {
        this.nazev = nazev;
    }

    public String getIc() {
        return ic;
    }

    public void setIc(String ic) {
        this.ic = ic;
    }

    public String getJineReg() {
        return jineReg;
    }

    public void setJineReg(String jineReg) {
        this.jineReg = jineReg;
    }

    public String getPravniRad() {
        return pravniRad;
    }

    public void setPravniRad(String pravniRad) {
        this.pravniRad = pravniRad;
    }

    public String getSidlo() {
        return sidlo;
    }

    public void setSidlo(String sidlo) {
        this.sidlo = sidlo;
    }
}
