package parser.model.EPD;

public class Pohledavka {
    private String cisloPrihlasky;
    private String cisloDilciPohledavky;
    private String veritel;
    private String dluznikPopira;

    public Pohledavka() {
    }

    public String getCisloPrihlasky() {
        return cisloPrihlasky;
    }

    public void setCisloPrihlasky(String cisloPrihlasky) {
        this.cisloPrihlasky = cisloPrihlasky;
    }

    public String getCisloDilciPohledavky() {
        return cisloDilciPohledavky;
    }

    public void setCisloDilciPohledavky(String cisloDilciPohledavky) {
        this.cisloDilciPohledavky = cisloDilciPohledavky;
    }

    public String getVeritel() {
        return veritel;
    }

    public void setVeritel(String veritel) {
        this.veritel = veritel;
    }

    public String getDluznikPopira() {
        return dluznikPopira;
    }

    public void setDluznikPopira(String dluznikPopira) {
        this.dluznikPopira = dluznikPopira;
    }
}
