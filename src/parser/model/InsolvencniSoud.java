package parser.model;

public class InsolvencniSoud {

    private String spisSoud;
    private String soud;
    private String senat;
    private String ins;
    private String cislo;
    private String rok;

    public InsolvencniSoud() {
    }

    public String getSpisSoud() {
        return spisSoud;
    }

    public void setSpisSoud(String spisSoud) {
        this.spisSoud = spisSoud;
    }

    public String getSoud() {
        return soud;
    }

    public void setSoud(String soud) {
        this.soud = soud;
    }

    public String getSenat() {
        return senat;
    }

    public void setSenat(String senat) {
        this.senat = senat;
    }

    public String getIns() {
        return ins;
    }

    public void setIns(String ins) {
        this.ins = ins;
    }

    public String getCislo() {
        return cislo;
    }

    public void setCislo(String cislo) {
        this.cislo = cislo;
    }

    public String getRok() {
        return rok;
    }

    public void setRok(String rok) {
        this.rok = rok;
    }

    @Override
    public String toString() {
        return "parser.model.InsolvencniSoud{" +
                "spisSoud='" + spisSoud + '\'' +
                ", soud='" + soud + '\'' +
                ", senat='" + senat + '\'' +
                ", ins='" + ins + '\'' +
                ", cislo='" + cislo + '\'' +
                ", rok='" + rok + '\'' +
                '}';
    }
}
