package parser.model;

public class FyzickaOsoba {
    private String spolecneOddluzeni;
    private String prijmeni;
    private String jmeno;
    private String datumRc;
    private String datumNarozeni;
    private String rodneCislo;
    private String ic;
    private String bydliste;
    private String zahranicni;
    private String statPrislustnost;

    public FyzickaOsoba() {
    }

    public String getSpolecneOddluzeni() {
        return spolecneOddluzeni;
    }

    public void setSpolecneOddluzeni(String spolecneOddluzeni) {
        this.spolecneOddluzeni = spolecneOddluzeni;
    }

    public String getPrijmeni() {
        return prijmeni;
    }

    public void setPrijmeni(String prijmeni) {
        this.prijmeni = prijmeni;
    }

    public String getJmeno() {
        return jmeno;
    }

    public void setJmeno(String jmeno) {
        this.jmeno = jmeno;
    }

    public String getDatumRc() {
        return datumRc;
    }

    public void setDatumRc(String datumRc) {
        this.datumRc = datumRc;
    }

    public String getDatumNarozeni() {
        return datumNarozeni;
    }

    public void setDatumNarozeni(String datumNarozeni) {
        this.datumNarozeni = datumNarozeni;
    }

    public String getRodneCislo() {
        return rodneCislo;
    }

    public void setRodneCislo(String rodneCislo) {
        this.rodneCislo = rodneCislo;
    }

    public String getIc() {
        return ic;
    }

    public void setIc(String ic) {
        this.ic = ic;
    }

    public String getBydliste() {
        return bydliste;
    }

    public void setBydliste(String bydliste) {
        this.bydliste = bydliste;
    }

    public String getZahranicni() {
        return zahranicni;
    }

    public void setZahranicni(String zahranicni) {
        this.zahranicni = zahranicni;
    }

    public String getStatPrislustnost() {
        return statPrislustnost;
    }

    public void setStatPrislustnost(String statPrislustnost) {
        this.statPrislustnost = statPrislustnost;
    }
}
