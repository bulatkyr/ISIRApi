package parser.model;

public class Identity {

    private FyzickaOsoba personIdentity;
    private FyzickaOsoba personManzeleIdentity;
    private PravnickaOsoba companyIdentity;

    public Identity() {
        personIdentity = new FyzickaOsoba();
        personManzeleIdentity = new FyzickaOsoba();
        companyIdentity = new PravnickaOsoba();
    }

    public FyzickaOsoba getPersonIdentity() {
        return personIdentity;
    }

    public void setPersonIdentity(FyzickaOsoba personIdentity) {
        this.personIdentity = personIdentity;
    }

    public FyzickaOsoba getPersonManzeleIdentity() {
        return personManzeleIdentity;
    }

    public void setPersonManzeleIdentity(FyzickaOsoba personManzeleIdentity) {
        this.personManzeleIdentity = personManzeleIdentity;
    }

    public PravnickaOsoba getCompanyIdentity() {
        return companyIdentity;
    }

    public void setCompanyIdentity(PravnickaOsoba companyIdentity) {
        this.companyIdentity = companyIdentity;
    }
}
