package parser;

import parser.model.EPD.Pohledavka;
import parser.model.Identity;
import parser.model.InsolvencniSoud;
import parser.model.podpis.Podpis;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class EPDParser extends AbstractXMLParser {

    private static final String POHLEDAVKA = "Pohledavka";
    private static final String CISLO_PRIHLASKY = "Cislo_prihlasky";
    private static final String CISLO_DILCI_POHLEDAVKY = "Cislo_dilci_pohledavky";
    private static final String VERITEL = "Veritel";
    private static final String DLUZNIK_POPIRA = "Dluznik_popira";

    @Override
    void processDocument(Document doc) {
        InsolvencniSoud insolvencniSoud = new InsolvencniSoud();
        String nadpisVerze = "";
        Identity dluznik = new Identity();
        Identity spravce = new Identity();
        List<Pohledavka> pohledavky = new ArrayList<>();
        String komentarText = "";
        Podpis podpis = new Podpis();

        Node nProhlaseniDluznika = doc.getElementsByTagName("Prohlaseni_dluznika").item(0);
        if (nProhlaseniDluznika != null) {
            Element eProhlaseniDluznika = (Element) nProhlaseniDluznika;
            Node nFormular = eProhlaseniDluznika.getElementsByTagName("Formular").item(0);

            if (nFormular != null) {
                Element eFormular = (Element) nFormular;
                Node nNadpis = eFormular.getElementsByTagName("Nadpis").item(0);

                if (nNadpis != null) {
                    Element eNadpis = (Element) nNadpis;
                    Node nInsolvencniSoud = eNadpis.getElementsByTagName("Insolvencni_soud").item(0);

                    if (nInsolvencniSoud != null) {
                        insolvencniSoud = Parser.parseInsolvencniSoud(nInsolvencniSoud);
                    }

                    Node nVerze = eNadpis.getElementsByTagName("Verze").item(0);
                    if (nVerze != null) {
                        nadpisVerze = nVerze.getTextContent();
                    }
                }
                Node nDluznik = eFormular.getElementsByTagName("Dluznik").item(0);
                if (nDluznik != null) {
                    dluznik = Parser.parsePersonIdentity(nDluznik);
                }
                Node nSpravce = eFormular.getElementsByTagName("Spravce").item(0);
                if (nSpravce != null) {
                    spravce = Parser.parsePersonIdentity(nSpravce);
                }
                Node nPohledavky = eFormular.getElementsByTagName("Pohledavky").item(0);
                if (nPohledavky != null) {
                    pohledavky = parsePohledavky(nPohledavky);
                }
                Node nKomentar = eFormular.getElementsByTagName("Komentar").item(0);
                if (nKomentar != null) {
                    Element eKomentar = (Element) nKomentar;
                    Node nKomentarText = eKomentar.getElementsByTagName("Komentar_text").item(0);
                    if (nKomentarText != null) {
                        komentarText = nKomentarText.getTextContent();
                    }
                }
                Node nPodpisy = eFormular.getElementsByTagName("Podpisy").item(0);
                if (nPodpisy != null) {
                    podpis = Parser.parsePodpisy(nPodpisy);
                }
            }
        }
    }

    public static List<Pohledavka> parsePohledavky(Node startNode) {
        List<Pohledavka> resultList = new ArrayList<>();
        Element eStartNode = (Element) startNode;

        NodeList pohledavkyList = eStartNode.getElementsByTagName(POHLEDAVKA);
        for (int i = 0; i < pohledavkyList.getLength(); i++) {
            Node item = pohledavkyList.item(i);
            if (item != null) {
                Pohledavka pohledavka = parsePohledavka(item);
                resultList.add(pohledavka);
            }
        }
        return resultList;
    }

    private static Pohledavka parsePohledavka(Node startNode) {
        Pohledavka pohledavka = new Pohledavka();
        Element eStartNode = (Element) startNode;

        Node nCisloPrihlasky = eStartNode.getElementsByTagName(CISLO_PRIHLASKY).item(0);
        if (nCisloPrihlasky != null) {
            pohledavka.setCisloPrihlasky(nCisloPrihlasky.getTextContent());
        }
        Node nCisloDilciPohledavky = eStartNode.getElementsByTagName(CISLO_DILCI_POHLEDAVKY).item(0);
        if (nCisloDilciPohledavky != null) {
            pohledavka.setCisloDilciPohledavky(nCisloDilciPohledavky.getTextContent());
        }
        Node nVeritel = eStartNode.getElementsByTagName(VERITEL).item(0);
        if (nVeritel != null) {
            pohledavka.setVeritel(nVeritel.getTextContent());
        }
        Node nDluznikPopira = eStartNode.getElementsByTagName(DLUZNIK_POPIRA).item(0);
        if (nDluznikPopira != null) {
            pohledavka.setDluznikPopira(nDluznikPopira.getTextContent());
        }
        return pohledavka;
    }
}
