package parser;

import parser.model.FyzickaOsoba;
import parser.model.Identity;
import parser.model.InsolvencniSoud;
import parser.model.PravnickaOsoba;
import parser.model.podpis.Podpis;
import parser.model.podpis.PodpisHlavicka;
import parser.model.podpis.PodpisOsoba;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class Parser {

    public static InsolvencniSoud parseInsolvencniSoud(Node insolvencniSoud) {
        InsolvencniSoud result = new InsolvencniSoud();
        Element eInsolvencniSoud = (Element) insolvencniSoud;

        Node spis = eInsolvencniSoud.getElementsByTagName("Spis").item(0);
        Element eSpis = (Element) spis;
        result.setSpisSoud(eSpis.getElementsByTagName("Soud").item(0).getTextContent());

        Node spisovaZnacka = eInsolvencniSoud.getElementsByTagName("Spisova_znacka").item(0);
        Element eSpisovaZnacka = (Element) spisovaZnacka;
        result.setSoud(eSpisovaZnacka.getElementsByTagName("Soud").item(0).getTextContent());
        result.setSenat(eSpisovaZnacka.getElementsByTagName("Senat").item(0).getTextContent());
        result.setIns(eSpisovaZnacka.getElementsByTagName("Ins").item(0).getTextContent());
        result.setCislo(eSpisovaZnacka.getElementsByTagName("Cislo").item(0).getTextContent());
        result.setRok(eSpisovaZnacka.getElementsByTagName("Rok").item(0).getTextContent());
        return result;
    }

    public static Identity parsePersonIdentity(Node personIdentity) {
        Identity result = new Identity();
        Element ePersonIdentity = (Element) personIdentity;

        Node fyzickaOsoba = ePersonIdentity.getElementsByTagName("Fyzicka_osoba").item(0);
        Element eFyzickaOsoba = (Element) fyzickaOsoba;

        Node fyzickaOsobaUdaje = eFyzickaOsoba.getElementsByTagName("Udaje").item(0);
        parseFyzickaOsobaUdaje(fyzickaOsobaUdaje, result.getPersonIdentity());

        Node fyzickaOsobaManzele = eFyzickaOsoba.getElementsByTagName("Manzele").item(0);
        if (fyzickaOsobaManzele != null) {
            parseFyzickaOsobaUdaje(fyzickaOsobaManzele, result.getPersonManzeleIdentity());
        }
        Node pravnickaOsoba = ePersonIdentity.getElementsByTagName("Pravnicka_osoba").item(0);
        Element ePravnickaOsoba = (Element) pravnickaOsoba;

        Node pravnickaOsobaUdaje = ePravnickaOsoba.getElementsByTagName("Udaje").item(0);
        parsePravnickaOsobaUdaje(pravnickaOsobaUdaje, result.getCompanyIdentity());

        return result;
    }

    public static void parsePravnickaOsobaUdaje(Node startNode, PravnickaOsoba result) {
        Element eStartNode = (Element) startNode;

        result.setZahranicni(eStartNode.getElementsByTagName("Zahranicni").item(0).getTextContent());
        result.setNazev(eStartNode.getElementsByTagName("Nazev").item(0).getTextContent());
        result.setIc(eStartNode.getElementsByTagName("IC").item(0).getTextContent());
        result.setJineReg(eStartNode.getElementsByTagName("Jine_reg").item(0).getTextContent());
        result.setPravniRad(eStartNode.getElementsByTagName("Pravni_rad").item(0).getTextContent());
        result.setSidlo(eStartNode.getElementsByTagName("Sidlo").item(0).getTextContent());
    }

    public static void parseFyzickaOsobaUdaje(Node startNode, FyzickaOsoba result) {
        Element eStartNode = (Element) startNode;

        NodeList spolecneOddluzeniList = eStartNode.getElementsByTagName("Spolecne_oddluzeni");
        if (spolecneOddluzeniList.item(0) != null) {
            result.setSpolecneOddluzeni(spolecneOddluzeniList.item(0).getTextContent());
        }

        result.setPrijmeni(eStartNode.getElementsByTagName("Prijmeni").item(0).getTextContent());
        result.setJmeno(eStartNode.getElementsByTagName("Jmeno").item(0).getTextContent());
        result.setDatumRc(eStartNode.getElementsByTagName("Datum_rc").item(0).getTextContent());
        result.setDatumNarozeni(eStartNode.getElementsByTagName("Datum_narozeni").item(0).getTextContent());
        result.setRodneCislo(eStartNode.getElementsByTagName("Rodne_cislo").item(0).getTextContent());
        result.setIc(eStartNode.getElementsByTagName("IC").item(0).getTextContent());
        result.setBydliste(eStartNode.getElementsByTagName("Bydliste").item(0).getTextContent());
        result.setZahranicni(eStartNode.getElementsByTagName("Zahranicni").item(0).getTextContent());
        result.setStatPrislustnost(eStartNode.getElementsByTagName("Stat_prislustnost").item(0).getTextContent());
    }

    public static Podpis parsePodpisy(Node startNode) {
        Podpis result = new Podpis();
        Element eStartNode = (Element) startNode;

        Node nHlavicka = eStartNode.getElementsByTagName("Hlavicka").item(0);
        if (nHlavicka != null) {
            parseHlavicka(nHlavicka, result.getHlavicka());
        }
        Node nIsfo = eStartNode.getElementsByTagName("IS_FO").item(0);
        if (nIsfo != null) {
            parsePodpisOsoba(nIsfo, result.getIsfo());
        }
        Node nDne = eStartNode.getElementsByTagName("Dne").item(0);
        if (nDne != null) {
            result.setDne(nDne.getTextContent());
        }
        Node nodeV = eStartNode.getElementsByTagName("V").item(0);
        if (nodeV != null) {
            result.setV(nodeV.getTextContent());
        }
        Node nDluznik = eStartNode.getElementsByTagName("Dluznik").item(0);
        if (nDluznik != null) {
            Element eDluznik = (Element) nDluznik;

            Node nDluznikFo = eStartNode.getElementsByTagName("Dluznik_FO").item(0);
            if (nDluznikFo != null) {
                parsePodpisOsoba(nDluznikFo, result.getDluznikFo());
            }
            Node nDluznikManzele = eStartNode.getElementsByTagName("Dluznik_Manzele").item(0);
            if (nDluznikManzele != null) {
                parsePodpisOsoba(nDluznikManzele, result.getDluznikManzele());
            }
        }
        return result;
    }

    public static void parseHlavicka(Node startNode, PodpisHlavicka result) {
        Element eStartNode = (Element) startNode;
        Node nodeV = eStartNode.getElementsByTagName("V").item(0);
        if (nodeV != null) {
            result.setV(nodeV.getTextContent());
        }
        Node nDne = eStartNode.getElementsByTagName("Dne").item(0);
        if (nDne != null) {
            result.setDne(nDne.getTextContent());
        }
    }

    public static void parsePodpisOsoba(Node startNode, PodpisOsoba result) {
        Element eStartNode = (Element) startNode;
        Node nJmeno = eStartNode.getElementsByTagName("Jmeno").item(0);
        if (nJmeno != null) {
            result.setJmeno(nJmeno.getTextContent());
        }
        Node nPrijmeni = eStartNode.getElementsByTagName("Prijmeni").item(0);
        if (nPrijmeni != null) {
            result.setPrijmeni(nPrijmeni.getTextContent());
        }
        Node nTitulPred = eStartNode.getElementsByTagName("Titul_pred").item(0);
        if (nTitulPred != null) {
            result.setTitulPred(nTitulPred.getTextContent());
        }
        Node nTitulZa = eStartNode.getElementsByTagName("Titul_za").item(0);
        if (nTitulZa != null) {
            result.setTitulZa(nTitulZa.getTextContent());
        }
        Node nPopisText = eStartNode.getElementsByTagName("Popis_text").item(0);
        if (nPopisText != null) {
            result.setPopisText(nPopisText.getTextContent());
        }
        Node nDne = eStartNode.getElementsByTagName("Dne").item(0);
        if (nDne != null) {
            result.setDne(nDne.getTextContent());
        }
        Node nodeV = eStartNode.getElementsByTagName("V").item(0);
        if (nodeV != null) {
            result.setV(nodeV.getTextContent());
        }
    }


}
