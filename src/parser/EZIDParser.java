package parser;

import parser.model.EZID.OsobniJednani;
import parser.model.Identity;
import parser.model.InsolvencniSoud;
import parser.model.podpis.Podpis;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;

public class EZIDParser extends AbstractXMLParser {

    @Override
    void processDocument(Document doc) {
        String nadpisVerze = "";
        InsolvencniSoud insolvencniSoud = new InsolvencniSoud();
        Identity dluznik = new Identity();
        Identity spravce = new Identity();
        OsobniJednani osobniJednani = new OsobniJednani();
        String komentarText = "";
        Podpis podpis = new Podpis();

        Node nZaznamOJednaniSDluznikem = doc.getElementsByTagName("Zaznam_o_jednani_s_dluznikem").item(0);
        if (nZaznamOJednaniSDluznikem != null) {
            Element eZaznamOJednaniSDluznikem = (Element) nZaznamOJednaniSDluznikem;
            Node nFormular = eZaznamOJednaniSDluznikem.getElementsByTagName("Formular").item(0);

            if (nFormular != null) {
                Element eFormular = (Element) nFormular;
                Node nNadpis = eFormular.getElementsByTagName("Nadpis").item(0);

                if (nNadpis != null) {
                    Element eNadpis = (Element) nNadpis;
                    Node nVerze = eNadpis.getElementsByTagName("Verze").item(0);

                    if (nVerze != null) {
                        nadpisVerze = nVerze.getTextContent();
                    }
                }
                Node nInsolvencniSoud = eFormular.getElementsByTagName("Insolvencni_soud").item(0);
                if (nInsolvencniSoud != null) {
                    insolvencniSoud = Parser.parseInsolvencniSoud(nInsolvencniSoud);
                }
                Node nDluznik = eFormular.getElementsByTagName("Dluznik").item(0);
                if (nDluznik != null) {
                    dluznik = Parser.parsePersonIdentity(nDluznik);
                }
                Node nSpravce = eFormular.getElementsByTagName("Spravce").item(0);
                if (nSpravce != null) {
                    spravce = Parser.parsePersonIdentity(nSpravce);
                }
                Node nOsobJednani = eFormular.getElementsByTagName("Osob_jednani").item(0);
                if (nOsobJednani != null) {
                    osobniJednani = parseOsobJednani(nOsobJednani);
                }
                //TODO KBU : tag <Zakon> was skipped and processed only tag <Komentar_text>
                Node nKomentarText = eFormular.getElementsByTagName("Komentar_text").item(0);
                if (nKomentarText != null) {
                    komentarText = nKomentarText.getTextContent();
                }
                Node nPodpisy = eFormular.getElementsByTagName("Podpisy").item(0);
                if (nPodpisy != null) {
                    podpis = Parser.parsePodpisy(nPodpisy);
                }
            }
        }
    }

    private static OsobniJednani parseOsobJednani(Node startNode) {
        OsobniJednani result = new OsobniJednani();
        if (startNode != null) {
            Element eStartNode = (Element) startNode;
            Node nUdaje = eStartNode.getElementsByTagName("Udaje").item(0);

            if (nUdaje != null) {
                Element eUdaje = (Element) nUdaje;
                Node nZacatek = eUdaje.getElementsByTagName("Zacatek").item(0);
                if (nZacatek != null) {
                    Element eZacatek = (Element) nZacatek;
                    Node nZacatekDatum = eZacatek.getElementsByTagName("Datum").item(0);
                    if (nZacatekDatum != null) {
                        result.setZacatekDatum(nZacatekDatum.getTextContent());
                    }
                    Node nZacatekCas = eZacatek.getElementsByTagName("Cas").item(0);
                    if (nZacatekCas != null) {
                        result.setZacatekCas(nZacatekCas.getTextContent());
                    }
                }
                Node nKonec = eUdaje.getElementsByTagName("Konec").item(0);
                if (nKonec != null) {
                    Element eKonec = (Element) nKonec;
                    Node nKonecDatum = eKonec.getElementsByTagName("Datum").item(0);
                    if (nKonecDatum != null) {
                        result.setKonecDatum(nKonecDatum.getTextContent());
                    }
                    Node nKonecCas = eKonec.getElementsByTagName("Cas").item(0);
                    if (nKonecCas != null) {
                        result.setKonecCas(nKonecCas.getTextContent());
                    }
                }
                Node nAdresaJednani = eUdaje.getElementsByTagName("Adresa_jednani").item(0);
                if (nAdresaJednani != null) {
                    result.setAdresaJednani(nAdresaJednani.getTextContent());
                }
                Node nZpusobZaznamu = eUdaje.getElementsByTagName("Zpusob_zaznamu").item(0);
                if (nZpusobZaznamu != null) {
                    Element eZpusobZaznamu = (Element) nZpusobZaznamu;
                    Node nSubZpusobZaznamu = eZpusobZaznamu.getElementsByTagName("Zpusob_zaznamu").item(0);
                    if (nSubZpusobZaznamu != null) {
                        result.setZpusobZaznamu(nSubZpusobZaznamu.getTextContent());
                    }
                    Node nPopisJine = eZpusobZaznamu.getElementsByTagName("Popis_jine").item(0);
                    if (nPopisJine != null) {
                        result.setPopisJine(nPopisJine.getTextContent());
                    }
                }
            }
        }
        return result;
    }

}
