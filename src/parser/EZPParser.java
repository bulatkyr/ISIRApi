package parser;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;
import parser.model.Identity;
import parser.model.InsolvencniSoud;
import parser.model.podpis.Podpis;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;

public class EZPParser extends AbstractXMLParser{

    @Override
    void processDocument(Document doc) {
        InsolvencniSoud insolvencniSoud = new InsolvencniSoud();
        String verze = "";
        Identity dluznik = new Identity();
        Identity spravce = new Identity();
        String komentarText = "";
        String prilohyPocitadlo = "";
        Podpis podpis = new Podpis();

        Node nZpravaOPrezkumu = doc.getElementsByTagName("Zprava_o_prezkumu").item(0);
        if (nZpravaOPrezkumu != null) {
            Element eZpravaOPrezkumu = (Element) nZpravaOPrezkumu;
            Node nFormular = eZpravaOPrezkumu.getElementsByTagName("Formular").item(0);

            if (nFormular != null) {
                Element eFormular = (Element) nFormular;
                Node nVerze = eFormular.getElementsByTagName("Verze").item(0);
                if (nVerze != null) {
                    verze = nVerze.getTextContent();
                }
                Node nInsolvencniSoud = eFormular.getElementsByTagName("Insolvencni_soud").item(0);
                if (nInsolvencniSoud != null) {
                    insolvencniSoud = Parser.parseInsolvencniSoud(nInsolvencniSoud);
                }
                Node nDluznik = eFormular.getElementsByTagName("Dluznik").item(0);
                if (nDluznik != null) {
                    dluznik = Parser.parsePersonIdentity(nDluznik);
                }
                Node nSpravce = eFormular.getElementsByTagName("Spravce").item(0);
                if (nSpravce != null) {
                    spravce = Parser.parsePersonIdentity(nSpravce);
                }
                Node nKomentar = eFormular.getElementsByTagName("Komentar").item(0);
                if (nKomentar != null) {
                    Element eKomentar = (Element) nKomentar;
                    Node nKomentar1 = eKomentar.getElementsByTagName("Komentar").item(0);
                    if (nKomentar1 != null) {
                        komentarText = nKomentar1.getTextContent();
                    }
                }
                Node nPodpisy = eFormular.getElementsByTagName("Podpisy").item(0);
                if (nPodpisy != null) {
                    podpis = Parser.parsePodpisy(nPodpisy);
                }
                //TODO KBU : Process tag <Prilohy>, AS-IS stav : processed only tag <Pocitadlo>
                Node nPrilohyPocitadlo = eFormular.getElementsByTagName("Pocitadlo").item(0);
                if (nPrilohyPocitadlo != null) {
                    prilohyPocitadlo = nPrilohyPocitadlo.getTextContent();
                }
            }
        }
    }
}
