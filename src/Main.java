import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import parser.*;
import parser.model.EPD.Pohledavka;
import parser.model.Identity;
import parser.model.podpis.Podpis;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.io.*;
import java.util.List;

public class Main {

    public static void main(String argv[]) throws ParserConfigurationException, IOException, SAXException, XMLStreamException {
        AbstractXMLParser parser = new EPDParser();
//        parseWithStaX();
//        parseWithDOM("testXml/EPLNPtest.xml");
//        parseEPLNP("testXml/EPLNPtest.xml");
//        testParseDluznik("testXml/testparseDluznik.xml");
//        testPodpisy("testXml/testPodpisy.xml");
//        testEpdPohledavky("testXml/testEpdPohledavky.xml");
        parser.parseDocument("testXml/EPDFullTest.xml");
        parser = new EZIDParser();
        parser.parseDocument("testXml/EZIDFullTest.xml");
        parser = new EZPParser();
        parser.parseDocument("testXml/EZPFullTest.xml");
    }


    public static void parseWithStaX() throws FileNotFoundException, XMLStreamException {
        XMLInputFactory inputFactory = XMLInputFactory.newInstance();
        InputStream in = new FileInputStream("testXml.xml");
        XMLStreamReader streamReader = inputFactory.createXMLStreamReader(in);
        streamReader.nextTag();
        streamReader.nextTag();

        int employees = 0;
        while (streamReader.hasNext()) {
            if (streamReader.isStartElement()) {
                switch (streamReader.getLocalName()) {
                    case "firstname": {
                        System.out.print("First Name : ");
                        System.out.println(streamReader.getElementText());
                        break;
                    }
                    case "lastname": {
                        System.out.print("Last Name : ");
                        System.out.println(streamReader.getElementText());
                        break;
                    }
                    case "nickname": {
                        System.out.print("Nickname : ");
                        System.out.println(streamReader.getElementText());
                        break;
                    }
                    case "salary": {
                        System.out.print("Salary : ");
                        System.out.println(streamReader.getElementText());
                        break;
                    }
                    case "staff": {
                        employees++;
                    }
                }
            }
            streamReader.next();
        }
        System.out.print(employees);
        System.out.println(" employees");
    }

    public static void parseWithDOM(String name) throws ParserConfigurationException, IOException, SAXException {
        File file = new File(name);
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        Document doc = dBuilder.parse(file);

        doc.getDocumentElement().normalize();

        NodeList nList = doc.getElementsByTagName("staff");

        for (int temp = 0; temp < nList.getLength(); temp++) {

            Node nNode = nList.item(temp);

            System.out.println("\nCurrent Element :" + nNode.getNodeName());

            if (nNode.getNodeType() == Node.ELEMENT_NODE) {

                Element eElement = (Element) nNode;

                System.out.println("Staff id : " + eElement.getAttribute("id"));
                System.out.println("First Name : " + eElement.getElementsByTagName("firstname").item(0).getTextContent());
                System.out.println("Last Name : " + eElement.getElementsByTagName("lastname").item(0).getTextContent());
                System.out.println("Nick Name : " + eElement.getElementsByTagName("nickname").item(0).getTextContent());
                System.out.println("Salary : " + eElement.getElementsByTagName("salary").item(0).getTextContent());

            }
        }
    }

    public static void parseEPLNP(String name) throws IOException, SAXException, ParserConfigurationException {
        File file = new File(name);
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        Document doc = dBuilder.parse(file);

        doc.getDocumentElement().normalize();

        NodeList nList = doc.getElementsByTagName("Prezkumny_list_nezajistene_pohledavky");

        Node nNode = nList.item(0);

        System.out.println("\nCurrent Element :" + nNode.getNodeName());

        if (nNode.getNodeType() == Node.ELEMENT_NODE) {
            Element eElement = (Element) nNode;
            NodeList formulars = eElement.getElementsByTagName("Formular");
            Node form = formulars.item(0);
            System.out.println("\nCurrent Element :" + form.getNodeName());

            if (form.getNodeType() == Node.ELEMENT_NODE) {
                Element eForm = (Element) form;
                NodeList insolvencniSouds = eForm.getElementsByTagName("Insolvencni_soud");
                Node insolvencniSoud = insolvencniSouds.item(0);
                System.out.println(Parser.parseInsolvencniSoud(insolvencniSoud).toString());
            }
        }
    }

    public static void testParseDluznik(String fileName) throws ParserConfigurationException, IOException, SAXException {
        File file = new File(fileName);
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        Document doc = dBuilder.parse(file);

        doc.getDocumentElement().normalize();

        Node dluznik = doc.getElementsByTagName("Dluznik").item(0);
        Identity dluznikResult = Parser.parsePersonIdentity(dluznik);
        System.out.println(dluznikResult);

        Node spravce = doc.getElementsByTagName("Spravce").item(0);
        Identity spravceResult = Parser.parsePersonIdentity(spravce);
        System.out.println(spravceResult);
    }

    public static void testPodpisy(String fileName) throws IOException, SAXException, ParserConfigurationException {
        File file = new File(fileName);
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        Document doc = dBuilder.parse(file);

        doc.getDocumentElement().normalize();

        Node podpisy = doc.getElementsByTagName("Podpisy").item(0);
        Podpis result = Parser.parsePodpisy(podpisy);
        System.out.println(result);

    }

    public static void testEpdPohledavky(String fileName) throws ParserConfigurationException, IOException, SAXException {
        File file = new File(fileName);
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        Document doc = dBuilder.parse(file);

        doc.getDocumentElement().normalize();

        Node pohledavky = doc.getElementsByTagName("Pohledavky").item(0);
        List<Pohledavka> result = EPDParser.parsePohledavky(pohledavky);
        System.out.println(result);

    }


}